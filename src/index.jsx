import React from "react";
import  ReactDOM  from "react-dom";
import "./components/styles/style.css"
import Header from "./components/header";
import Search from "./components/search";
import MenuItem from "./components/menu-item";
import dashboard from "./img/icons/dashboard.svg"
import revenue from "./img/icons/revenue.svg"
import notifications from "./img/icons/notifications.svg"
import analytics from "./img/icons/analytics.svg"
import inventory from "./img/icons/inventory.svg"
import logout from "./img/icons/logout.svg"
import LightModeItem from "./components/light-mode-item";
import arrow from "../src/img/icons/arrow.svg"
const App = () => {
 return (
   <>
     <Header></Header>
     <Search></Search>
    <img className="arrow" src={arrow} alt="arrow"></img>
         <MenuItem imgUrl={dashboard} text="Dashboard" hrefLink="#"></MenuItem>
         <MenuItem imgUrl={revenue} text="Revenue" hrefLink="#"></MenuItem>
         <MenuItem imgUrl={notifications}text="Notifications"hrefLink="#"></MenuItem>
         <MenuItem imgUrl={analytics} text="Analytics" hrefLink="#"></MenuItem>
         <MenuItem imgUrl={inventory} text="Inventory" hrefLink="#"></MenuItem>
         <MenuItem imgUrl={logout} text="Logout" hrefLink="#"></MenuItem>
     <LightModeItem></LightModeItem>
     
   </>
 );
}
ReactDOM.render(<App></App>, document.querySelector("#root"));