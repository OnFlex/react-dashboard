import React from "react";
import avatar from "../img/icons/avatar.png"

const Header = () => {
    return (
      <div className="header">
        <div className="user-avatar">
          <img src={avatar} alt="avatar"/>
        </div>
        <div className="log-email">
          <h2 className="user-login">AnimatedFred</h2>
          <p className="user-email">animated@demo.com</p>
        </div>
      </div>
    );
}
export default Header