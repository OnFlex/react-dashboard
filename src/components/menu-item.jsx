import React from "react";
import "./styles/style.css"


const MenuItem = (obj = {imgUrl:"",href:"",text: ""}) => {
    
    return (
      <div className="menu-item-box">
        <img src={obj.imgUrl} alt="menu-icon" />
        <a href={obj.hrefLink}>{obj.text}</a>
        {/* {<div className="switch-btn switch-on"></div>} */}
      </div>
    );
}
export default MenuItem