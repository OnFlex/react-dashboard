import React from "react";
import lightmode from "../img/icons/light mode.svg"
import darkmode from "../img/icons/darkmode.svg"
import choisesun from "../img/icons/choisesun.svg"

const LightModeItem = () => {
    return (
      <div className="menu-item-box">
        <img src={darkmode} alt="" />
        <a href="#">Darkmode</a>
        <label className="switch">
            <input type="checkbox"/>
            <span className="slider"><img className="lght" src={lightmode} alt="radio-icon" /></span>
           <img className="dark" src={choisesun} alt="radio-icon" />
        </label>
      </div>
    );
}
export default LightModeItem