import React from "react";
import search from "../img/icons/search.svg"

const Search = () => {
    return (
        <div className="search-box">
            <img src={search} alt="search-icon" />
           <input type="search" placeholder="Search..."/>
        </div>
    )
}
export default Search 